import React, { Component } from 'react'
import { Router } from '@reach/router';
import Test1 from '../components/test1';
import Test2 from '../components/test2';

export default class store extends Component {
  render() {
    return (
      <div>
        <Router>
          <Test1 path="/store" />
          <Test2 path="/store/:item" />
        </Router>
      </div>
    )
  }
}
